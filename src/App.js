import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import './App.css';
import fetcher from './fetcher';

import Header from './components/Header';

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Product from './pages/Product';
import Search from './pages/Search';
import Admin from './pages/Admin';
import PageNotFound from './pages/PageNotFound';
import { Container } from 'react-bootstrap';

import AdminProductAdd from './pages/AdminProductAdd';
import AdminProductEdit from './pages/AdminProductEdit';

import Account from './pages/Account';
import AccountOrders from './pages/AccountOrders';
import AccountProfile from './pages/AccountProfile';
import AccountPassword from './pages/AccountPassword';

import { useState, useEffect } from 'react';
import UserContext from './UserContext';


function App() {
  const [user, setUser] = useState(null);
  const unsetUser = () => {
    localStorage.clear();
    setUser(null);
  };

  const [cartNum, setCartNum] = useState('');
  const [wishlistNum, setWishlistNum] = useState('');

   useEffect(() => {
     document.title = 'ZU Mall';
   }, []);


  function updateUserInfo() {
    const userId = localStorage.getItem('userId');
    if (userId)
      fetcher.user(userId)
        .then(({status, data}) => {
          const tmp = data;
          tmp.id = data._id;

          if (status === 200) {
            setUser(tmp);
            setWishlistNum(data.wishlist.length);
            fetcher.cart()
              .then(({status, data}) => {
                if (status === 200)
                  setCartNum(data.products.length);
              })
          } else {
            setUser(null)
          }
        })
  }

  useEffect(() => {
    updateUserInfo();
  }, []);

  return (
    <Container
      className="App my-3"
      style={{ maxWidth: '1200px', margin: '0 auto' }}
    >
      <UserContext.Provider
        value={{
          user,
          setUser,
          unsetUser,
          wishlistNum,
          cartNum,
          updateUserInfo,
        }}
      >
        <Container>
          <Router>
            <Header />
            <Routes>
              <Route path="/" element={<Navigate to="/home" />} />
              <Route path="/home" element={<Home />} />
              <Route path="/home/:param1" element={<Home />} />
              <Route path="/home/:param1/:param2" element={<Home />} />
              <Route path="/product/:slug" element={<Product />} />

              <Route path="/account" element={<Account />} />
              <Route path="/account/:section" element={<Account />} />

              <Route path="/admin" element={<Admin />} />
              <Route path="/admin/:param1" element={<Admin />} />
              <Route path="/admin/:param1/:param2" element={<Admin />} />
              <Route path="/admin/:param1/:param2/:param3" element={<Admin />}/>

              <Route path="/register" element={<Register />} />
              <Route path="/login/:returnUrl" element={<Login />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="*" element={<PageNotFound />} />
            </Routes>
          </Router>
        </Container>
      </UserContext.Provider>
    </Container>
  );
}

export default App;

// vim: ft=javascriptreact
