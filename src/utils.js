import Swal from 'sweetalert2';

const dateOptions =
  {
    year: 'numeric',
    month: 'short',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
    hour12: false
  };

export function formatDate(isoDate) {
  return (new Date(isoDate)).toLocaleString('en-PH', dateOptions)
};

export function swalSuccess(title, timer) {
  return Swal.fire({
    title: title,
    icon: 'success',
    toast: true,
    timer: timer ? timer : 2000,
    timerProgressBar: true,
    position: 'top',
  })
}

export function swalError(title, timer) {
  return Swal.fire({
    title: title,
    icon: 'error',
    toast: true,
    timer: timer ? timer : 2000,
    timerProgressBar: true,
    position: 'top',
  })
}

export function calculateOrdersTotal(orders) {
  return orders.reduce((accumulator, current) =>
    accumulator + current.totalAmount, 0 )
}
