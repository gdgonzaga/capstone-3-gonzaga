import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));

const loadExternalLibs = () => {
  let lib = document.createElement("script");
  document.head.appendChild(lib);
  lib.setAttribute('src', 'https://kit.fontawesome.com/a4a5d70d32.js');
  lib.setAttribute('crossorigin', 'anonymous');

}

loadExternalLibs();

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
