
import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';
import ProductCard from './ProductCard';
import { CartContext } from '../pages/Cart';
import { swalError } from '../utils';

export default function WishlistProductCard({ product }) {
  const { price } = product;
  const [quantity, setQuantity] = useState(1);
  const { updateUserInfo } = useContext(UserContext);

  function modifyQuantity(amount) {
   let newVal = quantity + amount;
    newVal = (newVal === 0) ? 1 : newVal;
    newVal = (newVal > product.stock) ? product.stock : newVal;
    setQuantity(newVal);
  }

  function unWish() {
    fetcher.unWish(product._id)
      .then(({ status }) => {
        if (status === 200)
          updateUserInfo();
      });
  }

  function moveToCart() {
    fetcher.addToCart(product._id, quantity)
      .then(({ status }) => {
        if (status === 200)
          unWish();
      });
  }

  return (
    <>
      <Card>
        <Card.Body className="text-align-center center-children">
          <ProductCard product={product} />
        </Card.Body>
        <Card.Body className="border-top">
          {!product.isActive && (
            <>
              <Badge className="w-100" bg="danger">
                Not available
              </Badge>
            </>
          )}

          {product.isActive && (
            <>
              <div className="d-flex justify-content-center">
                <span className="mt-3">
                  <Button
                    variant="secondary"
                    className="px-3"
                    onClick={() => modifyQuantity(-1)}
                  >
                    <i className="fa-solid fa-minus"></i>
                  </Button>
                  <div className="quantity-indicator">{quantity}</div>
                  <Button
                    variant="secondary"
                    className="px-3"
                    onClick={() => modifyQuantity(1)}
                  >
                    <i className="fa-solid fa-plus"></i>
                  </Button>
                </span>
              </div>
              <div className="mt-1 d-flex justify-content-center">
                Total: &#8369; {(price * quantity).toLocaleString()}
              </div>
              <Button
                variant="success"
                className="w-100 my-2 add-to-wishlist"
                onClick={moveToCart}
              >
                Move to Cart
              </Button>
            </>
          )}

          <Button
            variant="danger"
            className="w-100 my-2 add-to-wishlist"
            onClick={unWish}
          >
            Remove
          </Button>
        </Card.Body>
      </Card>
    </>
  );
};

// vim: ft=javascriptreact
