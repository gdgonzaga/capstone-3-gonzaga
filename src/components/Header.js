import { InputGroup, Button, Form, Badge, Container, Image } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import HeaderButtons from './HeaderButtons';

export default function Header() {
  const navigate = useNavigate();

  const doSearch = (e) => {
    //e.preventDefault();

    const searchUrl = encodeURIComponent(e.target[0].value)
    setTimeout(5000)

    if (!searchUrl)
      return

    const href = `/home/search/${searchUrl}`;
    navigate(href);
  };

  return (
    <div className="container-fluid center-children mb-3">
      <div className="header-logo">
        <Link to="/">
          <Image src="/zuzu-logo.png" className="zuzu-logo" alt="zuzu-logo" />
        </Link>
      </div>

      <div className="header-search">
        <Form onSubmit={doSearch}>
          <InputGroup className="mb-3">
            <Form.Control
              placeholder="Search"
              aria-label="Search"
              aria-describedby="search-button"
            />
            <Button variant="secondary" id="search-button" type="submit">
              <i className="fas fa-search"></i>
            </Button>
          </InputGroup>
        </Form>
      </div>

      <div className="header-buttons">
        <HeaderButtons />
      </div>
    </div>
  );
}

// vim: ft=javascriptreact
      //<div className="header-search">
        //<form action="/products/search">
          //<div className="form-outline">
            //<input
              //className="search-form"
              //type="search"
              //placeholder="Search"
              //name="search" >
            //</input>
          //<Button type="button" variant="secondary" className="mx-2"><i className="fas fa-search"></i></Button>
        //</div>
        //</form>
      //</div>

