import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';

export default function ProfileEditField({field, defaultValue}) {
  return (
    <>
        <Form.Group className="mb-3">
          <Form.Label>First Name</Form.Label>
          <Form.Control
            type="text"
            defaultValue={firstName}
            onChange={e => setFirstName(e.target.value)}
          />
        </Form.Group>

        {isValid ?
          <Button variant="success" type="submit" onClick={sendUpdate}>
            Save changes
          </Button>
        :
          <Button variant="light" type="submit" disabled onClick={sendUpdate}>
            Save changes
          </Button>
        }
      </Form>
    </>
  )
  //return (
    //<>
      //<Form>
        //<Form.Group className="mb-3">
          //<Form.Label>Email address</Form.Label>
          //<Form.Control
            //type="email"
            //defaultValue={email}
            //onChange={e => setEmail(e.target.value)}
          ///>
        //</Form.Group>

        //<Form.Group className="mb-3">
          //<Form.Label>First Name</Form.Label>
          //<Form.Control
            //type="text"
            //defaultValue={firstName}
            //onChange={e => setFirstName(e.target.value)}
          ///>
        //</Form.Group>

        //<Form.Group className="mb-3">
          //<Form.Label>Last Name</Form.Label>
          //<Form.Control
            //type="text"
            //defaultValue={lastName}
            //onChange={e => setLastName(e.target.value)}
          ///>
        //</Form.Group>

        //<Form.Group className="mb-3">
          //<Form.Label>Mobile number</Form.Label>
          //<Form.Control
            //type="text"
            //onChange={e => setMobileNo(e.target.value)}
            //defaultValue={mobileNo}
          ///>
        //</Form.Group>

        //<Form.Group className="mb-3">
          //<Form.Label>Address</Form.Label>
          //<Form.Control
            //type="text"
            //defaultValue={address}
            //onChange={e => setAddress(e.target.value)}
          ///>
        //</Form.Group>

        //{isValid ?
          //<Button variant="success" type="submit" onClick={sendUpdate}>
            //Save changes
          //</Button>
        //:
          //<Button variant="light" type="submit" disabled onClick={sendUpdate}>
            //Save changes
          //</Button>
        //}
      //</Form>
    //</>
  //)
};

// vim: ft=javascriptreact
