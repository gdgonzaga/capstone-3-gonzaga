import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Collapse } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import fetcher from '../fetcher';
import { formatDate } from '../utils';
import { swalSuccess } from '../utils';
import { swalError } from '../utils';

import UserContext from '../UserContext';

import OrderProductLine from './OrderProductLine';

export default function OrderCard({ order: orderArg }) {
  const { user } = useContext(UserContext);
  const [order, setOrder] = useState(orderArg);
  const [userName, setUserName] = useState('');
  const [statusComponent, setStatusComponent] = useState(<></>);
  const [products, setProducts] = useState([]);
  const [actionButton, setActionButton] = useState('');

  const [purchasedComponent, setPurchasedComponent] = useState(<></>);
  const [packedComponent, setPackedComponent] = useState(<></>);
  const [shippedComponent, setShippedComponent] = useState(<></>);
  const [deliveredComponent, setDeliveredComponent] = useState(<></>);
  const [refundRequestedComponent, setRefundRequestedComponent] = useState(<></>);
  const [refundApprovedComponent, setRefundApprovedComponent] = useState(<></>);
  const [returnedComponent, setReturnedComponent] = useState(<></>);

  const [productsComponent, setProductsComponent] = useState([]);
  const [borderColorClass, setBorderColorClass] = useState('');

  function setReturned() {
    fetcher.returned(order._id)
      .then(({status, data}) => {
        if (!data) {
          swalError('Cannot set item as returned')
          return;
        }

        setOrder(data);
        swalSuccess('Item returned')
      })
  }

  function setRefundApproved() {
    fetcher.refundApproved(order._id)
      .then(({status, data}) => {
        if (!data) {
          swalError('Cannot approve refund')
          return;
        }

        setOrder(data);
        swalSuccess('Refund approved')
      })
  }

  function setRefundRequested() {
    fetcher.refundRequested(order._id)
      .then(({status, data}) => {
        if (!data) {
          swalError('Cannot request refund')
          return;
        }

        setOrder(data);
        swalSuccess('Refund requested')
      })
  }

  function setDelivered() {
    fetcher.delivered(order._id)
      .then(({status, data}) => {
        if (!data) {
          swalError('Cannot set item as delivered')
          return;
        }

        setOrder(data);
        swalSuccess('Item delivered')
      })
  }

  function setShipped() {
    fetcher.shipped(order._id)
      .then(({status, data}) => {
        if (!data) {
          swalError('Cannot set item as shipped')
          return;
        }

        setOrder(data);
        swalSuccess('Item shipped')
      })
  }

  function setPacked() {
    fetcher.packed(order._id)
      .then(({status, data}) => {
        if (!data) {
          swalError('Cannot set item as packed')
          return;
        }

        setOrder(data);
        swalSuccess('Item packed')
      })
  }

  function updateStatusComponent() {
    setActionButton(<></>)

    if (order.returnedOn) {
      setBorderColorClass('border-dark')
      setStatusComponent(<Badge bg="dark">Returned</Badge>)
    }
    else  if (order.refundApprovedOn) {
      setBorderColorClass('border-secondary')
      setStatusComponent(<Badge bg="secondary">Refund approved</Badge>)
      if (user.isAdmin)
        setActionButton (
          <Button onClick={setReturned} variant="primary" className="order-action-button">
          Returned</Button>
        )
    }
    else  if (order.refundRequestedOn) {
      setBorderColorClass('border-danger')
      setStatusComponent(<Badge bg="danger">Refund requested</Badge>)
      if (user.isAdmin)
        setActionButton (
          <Button onClick={setRefundApproved} variant="primary" className="order-action-button">
          Approve refund</Button>
        )
    }
    else  if (order.deliveredOn) {
      setBorderColorClass('border-success')
      setStatusComponent(<Badge bg="success">Delivered</Badge>)
      if (!user.isAdmin)
        setActionButton (
          <Button onClick={setRefundRequested} variant="primary" className="order-action-button">
          Request refund</Button>
        )
    }
    else  if (order.shippedOn) {
      setBorderColorClass('border-secondary')
      setStatusComponent(<Badge bg="secondary">Shipped</Badge>)
      if (user.isAdmin)
        setActionButton (
          <Button onClick={setDelivered} variant="primary" className="order-action-button">
          Delivered</Button>
        )
    }
    else  if (order.packedOn) {
      setBorderColorClass('border-secondary')
      setStatusComponent(<Badge bg="secondary">Packed</Badge>)
      if (user.isAdmin)
        setActionButton (
          <Button onClick={setShipped} variant="primary" className="order-action-button">Shipped</Button>
        )
    }
    else  if (order.purchasedOn) {
      setBorderColorClass('border-warning')
      setStatusComponent(<Badge bg="warning">Pending</Badge>)
      if (user.isAdmin)
        setActionButton (
          <Button onClick={setPacked} variant="primary" className="order-action-button">Packed</Button>
        )
    }
  }

  function updateDates() {
    if (order.purchasedOn)
      setPurchasedComponent(<li><strong>Purchased:</strong> {formatDate(order.purchasedOn)}</li>)
    if (order.packedOn)
      setPackedComponent(<li><strong>Packed:</strong> {formatDate(order.packedOn)}</li>)
    if (order.shippedOn)
      setShippedComponent(<li><strong>Shipped:</strong> {formatDate(order.shippedOn)}</li>)
    if (order.deliveredOn)
      setDeliveredComponent(<li><strong>Delivered:</strong> {formatDate(order.deliveredOn)}</li>)
    if (order.refundRequestedOn)
      setRefundRequestedComponent(<li><strong>Refund requested:</strong> {formatDate(order.refundRequestedOn)}</li>)
    if (order.refundApprovedOn)
      setRefundApprovedComponent(<li><strong>Refund approved:</strong> {formatDate(order.refundApprovedOn)}</li>)
    if (order.returnedOn)
      setReturnedComponent(<li><strong>Returned:</strong> {formatDate(order.returnedOn)}</li>)
  };

  async function updateProducts() {
    setProductsComponent(order.products.map(op =>
      <div key={op.productId} style={{maxWidth:"300px", minWidth: "300px"}} className="m-3">
        <OrderProductLine product={op} orderId={order._id}/>
      </div>
    ));
  };

  useEffect(() => {
    fetcher.user(order.userId)
      .then(({status, data}) => {
        setUserName(`${data.firstName} ${data.lastName}`);
      });
  }, []);

  useEffect(() => {
    updateStatusComponent();
    updateDates();
    updateProducts();
  }, [order]);

  useEffect(() => {
  }, [productsComponent]);

  const showProductComponent
    = <>Show products<i className="fa-solid fa-angles-down" style={{marginLeft: "0.5rem"}}></i></>
  const hideProductComponent
    = <>Hide products<i className="fa-solid fa-angles-up" style={{marginLeft: "0.5rem"}}></i></>
  const [open, setOpen] = useState(false)
  const [productButtonLabel, setProductbuttonlabel] = useState(showProductComponent)
  function toggleProducts() {
    const curOpen = open;
    setOpen(!open);
    curOpen ?
      setProductbuttonlabel(showProductComponent)
      :
      setProductbuttonlabel(hideProductComponent)
  }

  return (
    <Container>
      <div className={`border ${borderColorClass} rounded mb-3 p-3`}>
        <div className="d-flex flex-wrap justify-content-around">
          <div className="m-2 p-2">
            <ul className="list-unstyled">
              <li>
                <strong>Order#:</strong>
                <small>{order._id}</small>
              </li>
              {user.isAdmin && (
                <li>
                  <strong>User</strong>:
                  <Link to={`/admin/users/${order.userId}`}>{userName}</Link>
                </li>
              )}
              <li>
                <strong>Total:</strong> &#8369;{' '}
                {order.totalAmount.toLocaleString()}
              </li>
              <li>
                <strong>Status:</strong> {statusComponent}{' '}
              </li>
              <li>{actionButton}</li>
            </ul>
          </div>
          <div md={6} lg={4} xl={3} className="m-2 p-2">
            <ul className="list-unstyled">
              {purchasedComponent}
              {packedComponent}
              {shippedComponent}
              {deliveredComponent}
              {refundRequestedComponent}
              {refundApprovedComponent}
              {returnedComponent}
            </ul>
          </div>
        </div>
        <Button
          variant="light"
          onClick={toggleProducts}
          aria-controls="example-collapse-text"
          aria-expanded={open}
          className="container-fluid"
        >
          {productButtonLabel}
        </Button>
        <Collapse in={open}>
          <div id="example-collapse-text" className="mt-3">
            <div className="d-flex justify-content-center flex-wrap">
              {productsComponent}
            </div>
          </div>
        </Collapse>
      </div>
    </Container>
  );
};

// vim: ft=javascriptreact
