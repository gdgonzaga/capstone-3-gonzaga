import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';
import { calculateOrdersTotal } from '../utils';

export default function AdminUserLine({ user }) {
  const [totalAmount, setTotalAmount] = useState('');

  useEffect(() => {
    fetcher.userOrders(user._id)
      .then(({status, data}) => {
        if (status === 200) {
          setTotalAmount(calculateOrdersTotal(data));
        }
      })
  }, []);

  return (
    <>
      <Link to={`/admin/users/${user._id}`}>{user.firstName} {user.lastName}</Link>
      {user.isAdmin ?
        <Badge bg="danger" className="mx-2">Admin</Badge>
      :
        <span><strong className="mx-2">Total spent:</strong>&#8369; {totalAmount.toLocaleString()}</span>
      }
      
    </>
  )
};

// vim: ft=javascriptreact
