import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';
import Stars from '../components/Stars';

export default function ProductReviews({ ratings }) {

  function getReviews() {
    return ratings.map((rating) => (
        <div className="d-flex flex-column justify-content-center m-3" style={{ width: '200px' }}>
          <div className="w-100 d-flex justify-content-center"><Stars rating={rating.stars} /></div>
          <div className="w-100 d-flex justify-content-center"><span>{rating.comment}</span></div>
        </div>
    ));
  }

  return (
    <div className="d-flex justify-content-center flex-wrap align-items-start">
      {getReviews()}
    </div>
  )
}

// vim: ft=javascriptreact
