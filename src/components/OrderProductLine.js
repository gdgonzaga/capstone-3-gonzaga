import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';
import { Modal } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';
import AccountOrderRating from './AccountOrderRating';

import ProductCard from '../components/ProductCard';

export default function OrderProductLine(props) {
  const { productId, quantity, ratedOn, comment } = props.product;
  const { orderId } = props;
  const { user } = useContext(UserContext);

  const [component, setComponent] = useState();

  const [product, setProduct] = useState(null);

  function updateProduct() {
    fetcher.product(productId)
      .then(({status, data}) => {
        setProduct(data);
      })
  }

  useEffect(() => {
    updateProduct();
  }, []);

  useEffect(() => {
    if (!product)
      return

    setComponent(
      <div>
        <Card className="p-0">
          <Card.Header>
            {quantity} {quantity === 1 ? <>piece</> : <>pieces</>}
            <span style={{ display: 'inline-block', float: 'right' }}>
              &#8369; {(quantity * product.price).toLocaleString()}
            </span>
          </Card.Header>
          <Card.Body>
            {product._id && <ProductCard product={product} />}
          </Card.Body>
          {!user.isAdmin && (
            <>
              <AccountOrderRating
                product={product}
                orderId={orderId}
                ratedOn={ratedOn}
              />
            </>
          )}
        </Card>
      </div>
    );
  }, [product]);


  return (
    <>{component}</>
  )
};

// vim: ft=javascriptreact
