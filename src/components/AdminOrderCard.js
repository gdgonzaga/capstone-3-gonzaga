import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';
import { formatDate } from '../utils';
import { swalSuccess } from '../utils';
import { swalError } from '../utils';

import ProductCard from './ProductCard';
import OrderProductLine from './OrderProductLine';

import Swal from 'sweetalert2';

export default function AdminOrderCard({order: orderArg}) {

  const [order, setOrder] = useState(orderArg);
  const [userName, setUserName] = useState('');
  const [statusComponent, setStatusComponent] = useState(<></>);
  const [products, setProducts] = useState([]);
  const [actionButton, setActionButton] = useState('');

  const [purchasedComponent, setPurchasedComponent] = useState(<></>);
  const [packedComponent, setPackedComponent] = useState(<></>);
  const [shippedComponent, setShippedComponent] = useState(<></>);
  const [deliveredComponent, setDeliveredComponent] = useState(<></>);
  const [refundRequestedComponent, setRefundRequestedComponent] = useState(<></>);
  const [refundApprovedComponent, setRefundApprovedComponent] = useState(<></>);
  const [returnedComponent, setReturnedComponent] = useState(<></>);

  const [productsComponent, setProductsComponent] = useState([]);
  const [borderColorClass, setBorderColorClass] = useState('');

  function setReturned() {
    fetcher.returned(order._id)
      .then(({status, data}) => {
        if (!data) {
          swalError('Cannot set item as returned')
          return;
        }

        setOrder(data);
        swalSuccess('Item returned')
      })
  }

  function setRefundApproved() {
    fetcher.refundApproved(order._id)
      .then(({status, data}) => {
        if (!data) {
          swalError('Cannot approve refund')
          return;
        }

        setOrder(data);
        swalSuccess('Refund approved')
      })
  }

  function setRefundRequested() {
    fetcher.refundRequested(order._id)
      .then(({status, data}) => {
        if (!data) {
          swalError('Cannot request refund')
          return;
        }

        setOrder(data);
        swalSuccess('Refund requested')
      })
  }

  function setDelivered() {
    fetcher.delivered(order._id)
      .then(({status, data}) => {
        if (!data) {
          swalError('Cannot set item as delivered')
          return;
        }

        setOrder(data);
        swalSuccess('Item delivered')
      })
  }

  function setShipped() {
    fetcher.shipped(order._id)
      .then(({status, data}) => {
        if (!data) {
          swalError('Cannot set item as shipped')
          return;
        }

        setOrder(data);
        swalSuccess('Item shipped')
      })
  }

  function setPacked() {
    fetcher.packed(order._id)
      .then(({status, data}) => {
        if (!data) {
          swalError('Cannot set item as packed')
          return;
        }

        setOrder(data);
        swalSuccess('Item packed')
      })
  }

  function updateStatusComponent() {
    if (order.returnedOn) {
      setStatusComponent(<Badge bg="dark">Returned</Badge>)
      setActionButton(
        <></>
      )
    }
    else  if (order.refundApprovedOn) {
      setStatusComponent(<Badge bg="secondary">Refund approved</Badge>)
      setActionButton()
    }
    else  if (order.refundRequestedOn) {
      setStatusComponent(<Badge bg="danger">Refund requested</Badge>)
      setActionButton (
        <Button onClick={setRefundApproved} variant="primary" className="order-action-button">
        Approve refund</Button>
      )
    }
    else  if (order.deliveredOn) {
      setStatusComponent(<Badge bg="success">Delivered</Badge>)
      setActionButton ()
    }
    else  if (order.shippedOn) {
      setStatusComponent(<Badge bg="secondary">Shipped</Badge>)
      setActionButton (
        <Button onClick={setDelivered} variant="primary" className="order-action-button">
        Delivered</Button>
      )
    }
    else  if (order.packedOn) {
      setStatusComponent(<Badge bg="secondary">Packed</Badge>)
      setActionButton (
        <Button onClick={setShipped} variant="primary" className="order-action-button">Shipped</Button>)
    }
    else  if (order.purchasedOn) {
      setStatusComponent(<Badge bg="warning">Pending</Badge>)
      setActionButton (
        <Button onClick={setPacked} variant="primary" className="order-action-button">Packed</Button>
      )
    }
  }

  function updateDates() {
    if (order.purchasedOn)
      setPurchasedComponent(<div>Purchased: {formatDate(order.purchasedOn)}</div>)
    if (order.packedOn)
      setPackedComponent(<div>Packed: {formatDate(order.packedOn)}</div>)
    if (order.shippedOn)
      setShippedComponent(<div>Shipped: {formatDate(order.shippedOn)}</div>)
    if (order.deliveredOn)
      setDeliveredComponent(<div>Delivered: {formatDate(order.deliveredOn)}</div>)
    if (order.refundRequestedOn)
      setRefundRequestedComponent(<div>Refund requested: {formatDate(order.refundRequestedOn)}</div>)
    if (order.refundApprovedOn)
      setRefundApprovedComponent(<div>Refund approved: {formatDate(order.refundApprovedOn)}</div>)
    if (order.returnedOn)
      setReturnedComponent(<div>Returned: {formatDate(order.returnedOn)}</div>)
  };

  async function updateProducts() {
    setProductsComponent(order.products.map(op =>
      <Col sm={6} md={4} lg={3} key={op.productId}>
        <OrderProductLine product={op} />
      </Col>
    ));
  };

  useEffect(() => {
    fetcher.user(order.userId)
      .thej(({status, data}) => {
        setUserName(`${data.firstName} ${data.lastName}`);
      });
  }, []);

  useEffect(() => {
    updateStatusComponent();
    updateDates();
    updateProducts();
  }, [order]);

  useEffect(() => {
  }, [productsComponent]);

  return (
    <>
    <h1>admin order card</h1>
      <Card className="p-2 m-3 product-card" style={{minWidth: "250px"}} as={Link}>
        <Card.Img className="product-card-image" variant="top" />
        <Card.Body>
          <div>Order#: {order._id}</div>
          <div>User: <Link to={`/admin/users/${order.userId}`}>{userName}</Link></div>
          <div>Status: {statusComponent} </div>
          {purchasedComponent}
          {packedComponent}
          {shippedComponent}
          {deliveredComponent}
          {refundRequestedComponent}
          {refundApprovedComponent}
          {returnedComponent}
          <div>{actionButton}</div>
          <Card.Text>
            {productsComponent}
          </Card.Text>
        </Card.Body>
      </Card>
    </>
  )
};

// vim: ft=javascriptreact
