import { useState } from 'react';
import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { Card } from 'react-bootstrap';
import { Modal } from 'react-bootstrap';

import { Form } from 'react-bootstrap';

import fetcher from '../fetcher';

export default function AccountOrderRating(props) {
  const productId = props.product._id;
  const orderId = props.orderId;
  const product = props.product;

  const [comment, setComment] = useState('');
  const [ratedOn, setRatedOn] = useState(props.ratedOn);
  const [stars, setStars] = useState(5);
  const [orderStatus, setOrderStatus] = useState('');

  function updateOrderStatus() {
    fetcher.order(orderId)
      .then(({status, data: order}) => {
        if (order) {
            if (order.returnedOn) {
              //console.log('ret', order.returnedOn);
              setOrderStatus('returned')
            }
            else if (order.refundApprovedOn) {
              //console.log('ref', order.refundApprovedOn)
              setOrderStatus('refundApproved')
            }
            else if (order.refundRequestedOn) {
              //console.log('req', order.refundRequestedOn);
              setOrderStatus('refundRequested')
            }
            else if (order.deliveredOn) {
              //console.log('deliv', order.deliveredOn);
              setOrderStatus('delivered')
            }
            else if (order.shippedOn) {
              //console.log('ship', order.shippedOn);
              setOrderStatus('shipped')
            }
            else if (order.packedOn) {
              //console.log('pack', order.packedOn);
              setOrderStatus('packed')
            }
            else if (order.purchasedOn) {
              //console.log('pur', order.purchasedOn);
              setOrderStatus('pending')
            }
        }
      })
  }

  useEffect(() => {
    let ratingObj = null;
    for (let i = 0; i < product.ratings.length; i++) {
      if (product.ratings[i].orderId === orderId)
        ratingObj = product.ratings[i];
    }

    if (ratingObj) {
      setStars(ratingObj.stars);
      if (ratingObj.comment)
        setComment(ratingObj.comment)
    }

    updateOrderStatus();


  }, []);

  const userRatingComponent =
        <>
          <div className="container-fluid d-flex flex-column justify-content-center">
            <div className="d-flex justify-content-center"><h5>Your rating</h5></div>
            <div className="d-flex justify-content-center text-warning my-2">
                {stars >= 1 ?
                <i className="fa-solid fa-star"></i> :
              <i className="fa-regular fa-star"></i>}
                {stars >= 2 ?
                <i className="fa-solid fa-star"></i> :
              <i className="fa-regular fa-star"></i>}
                {stars >= 3 ?
                <i className="fa-solid fa-star"></i> :
              <i className="fa-regular fa-star"></i>}
                {stars >= 4 ?
                <i className="fa-solid fa-star"></i> :
              <i className="fa-regular fa-star"></i>}
                {stars >= 5 ?
                <i className="fa-solid fa-star"></i> :
              <i className="fa-regular fa-star"></i>}
            </div>
            <div className="d-flex justify-content-center">{comment}</div>
          </div>
        </>

  const [showModal, setShowModal] = useState(false);
  function openModal() { setShowModal(true);}
  function closeModal() { setShowModal(false); }


  const [reviewSent, setReviewSent] = useState(false);
  function submit(e) {
    e.preventDefault();
    const comment = e.target[0].value;
    fetcher.rate(productId, orderId, stars, comment)
      .then(({ status, data }) => {
        setRatedOn(data.ratedOn)
        setStars(data.stars);
        setComment(data.comment);
        setReviewSent(true);
      })
    closeModal()
  }

  const reviewModal =
      <Modal show={showModal}>
        <Modal.Header>
          <Modal.Title>Review product</Modal.Title>
        </Modal.Header>
        <Form onSubmit={submit}>
          <Modal.Body>
            <div>
              <Link className="star text-warning" onClick={() => setStars(1)}>
                {stars >= 1 ? (
                  <i className="fa-solid fa-star"></i>
                ) : (
                  <i className="fa-regular fa-star"></i>
                )}
              </Link>

              <Link className="star text-warning" onClick={() => setStars(2)}>
                {stars >= 2 ? (
                  <i className="fa-solid fa-star"></i>
                ) : (
                  <i className="fa-regular fa-star"></i>
                )}
              </Link>

              <Link className="star text-warning" onClick={() => setStars(3)}>
                {stars >= 3 ? (
                  <i className="fa-solid fa-star"></i>
                ) : (
                  <i className="fa-regular fa-star"></i>
                )}
              </Link>

              <Link className="star text-warning" onClick={() => setStars(4)}>
                {stars >= 4 ? (
                  <i className="fa-solid fa-star"></i>
                ) : (
                  <i className="fa-regular fa-star"></i>
                )}
              </Link>

              <Link className="star text-warning" onClick={() => setStars(5)}>
                {stars >= 5 ? (
                  <i className="fa-solid fa-star"></i>
                ) : (
                  <i className="fa-regular fa-star"></i>
                )}
              </Link>
            </div>
            <Form.Control as="textarea" rows={5} defaultValue={' '} className="mt-3"/>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={closeModal}>
              Cancel
            </Button>
            <Button variant="primary" type="submit">
              Save Changes
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>

  const [ratingComponent, setRatingComponent] = useState(<></>);

  useEffect(() => {
    if (!orderStatus) {
      return;
    }

    if (ratedOn)
      setRatingComponent(userRatingComponent)
    else if
      (
        orderStatus !== 'pending' &&
        orderStatus !== 'packed' &&
        orderStatus !== 'shipped'
      )
      setRatingComponent(<Button onClick={openModal}>Leave a review</Button>);
  }, [orderStatus, reviewSent]);

  return (
    <>
      {ratingComponent}
      {reviewModal}
    </>
  );
}

// vim: ft=javascriptreact
