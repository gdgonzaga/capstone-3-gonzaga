import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';
import ProductCard from './ProductCard';
import { CartContext } from '../pages/Cart';
import { swalError } from '../utils';

export default function CartProductCard({ product, quantity: quantityArg }) {
  const { price } = product;
  const [quantity, setQuantity] = useState(quantityArg);
  const { updateCart } = useContext(CartContext);
  const { updateUserInfo } = useContext(UserContext);

  function modifyQuantity(amount) {
   let newVal = quantity + amount;
    newVal = (newVal === 0) ? 1 : newVal;
    newVal = (newVal > product.stock) ? product.stock : newVal;

    fetcher.addToCart(product._id, newVal)
      .then(({ status }) => {
        if (status === 200) {
          setQuantity(newVal);
          updateUserInfo();
          updateCart();
        }
      })
  }

  function removeProduct() {
    fetcher.addToCart(product._id, 0).then(({ status }) => {
        if (status === 200) {
          updateCart();
          updateUserInfo();
        }
      })
  }

  function moveToWishlist() {
    fetcher.addToWishlist(product._id)
      .then(({status, data}) => {
        if (status === 200)
          removeProduct();
        else
          swalError(data.message, 5000);
      })
  }

  return (
      <Card>
    <Card.Body className="text-align-center center-children">
        <ProductCard product={product} />

    </Card.Body>
    <Card.Body className="border-top">
              <div className="d-flex justify-content-center">
      <span className="mt-3">
          <Button variant="secondary" className="px-3" onClick={() => modifyQuantity(-1)}>
            <i className="fa-solid fa-minus"></i>
          </Button>
          <div className="quantity-indicator">{quantity}</div>
          <Button variant="secondary" className="px-3" onClick={() => modifyQuantity(1)}>
            <i className="fa-solid fa-plus"></i>
          </Button>
      </span>
    </div>
              <div className="mt-1 d-flex justify-content-center">
      <div className="mt-1">Total: &#8369; {(price * quantity).toLocaleString()}</div>
    </div>

    <Button variant="warning" className="my-2 add-to-wishlist w-100" onClick={moveToWishlist}>
      Move to wishlist
    </Button>

    <Button variant="danger" className="my-2 add-to-wishlist w-100" onClick={removeProduct}>
      Remove
    </Button>

    </Card.Body>
      </Card>
  )
};

// vim: ft=javascriptreact
