import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import { swalSuccess } from '../utils';
import { swalError } from '../utils';

import UserContext from '../UserContext';
import fetcher from '../fetcher';

export default function ProductActions({ returnUrl, product }) {
  const loginLink = `/login/${returnUrl}`;

  const {user, updateUserInfo} = useContext(UserContext);
  const [quantity, setQuantity] = useState(1);

  const addQuantity = () => {
    setQuantity(quantity + 1);
  };

  const subtractQuantity = () => {
    if (quantity > 1)
      setQuantity(quantity - 1);
  };

  function addToCart() {
    fetcher.addToCart(product._id, quantity)
      .then(({ status }) => {
        if (status === 200) {
          swalSuccess('Added to cart');
          updateUserInfo();
        } else
          swalError('Cannot add to wishlist')
      });
  }

  function addToWishlist() {
    fetcher.addToWishlist(product._id)
      .then(({ status, data }) => {
        if (status === 200) {
          swalSuccess('Added to wishlist');
          setItemIsWished(true);
          updateUserInfo();
        } else
          swalError(data.message, 5000);
      });
  }

  function removeFromWishlist() {
    fetcher.unWish(product._id)
      .then(({ status, data }) => {
        if (status === 200) {
          swalSuccess('Removed from wishlist');
          updateUserInfo();
          setItemIsWished(false);
        } else
          swalError(data.message, 5000);
      });
  }

  let productActions = <></>;

  function getItemIsWished() {
    if (!user)
      return;

    return user.wishlist
      .map(w => w.productId)
      .includes(product._id)
  }

  const [itemIsWished, setItemIsWished] = useState(getItemIsWished());

  useEffect(() => {
    setItemIsWished(getItemIsWished());
  }, [user]);

  if (user && !user.isAdmin && product && !product.isActive)
    <></>
  else if (user && !user.isAdmin)
  productActions =
      <>
        <Button variant="secondary" className="px-4" onClick={subtractQuantity}>
          <i className="fa-solid fa-minus"></i>
        </Button>

        <div className="quantity-indicator">{quantity}</div>

        <Button variant="secondary" className="px-4" onClick={addQuantity}>
          <i className="fa-solid fa-plus"></i>
        </Button><br />

        <Button variant="primary" className="my-2 add-to-cart" onClick={addToCart}>
          <i className="fa-solid fa-cart-plus" style={{height: "2rem", fontSize: "2rem"}}></i>
        </Button>&nbsp;&nbsp;&nbsp;

        {itemIsWished ?
          <Button variant="light" className="my-2 add-to-wishlist" onClick={removeFromWishlist}>
            <i className="fa-solid fa-heart text-danger" style={{height: "2rem", fontSize: "2rem"}}></i>
            </Button>
        :
          <Button variant="light" className="my-2 add-to-wishlist" onClick={addToWishlist}>
            <i className="fa-regular fa-heart text-dark" style={{height: "2rem", fontSize: "2rem"}}></i>
            </Button>
        }
      </>
  else if (user && user.isAdmin)
    productActions =
      <>
        <Badge bg="light" text="dark" className="p-3 my-2">Admin users are not allowed to place orders.</Badge> <br />
        <Button as={Link} to={`/admin/products/edit/${product._id}`}>Edit product</Button>
      </>
  else if (!user)
    productActions =
      <Button variant="primary" className="my-2" as={Link} to={loginLink}>Log in to place an order</Button>
  return (
    <>
      {productActions}
    </>
  )
};

// vim: ft=javascriptreact
