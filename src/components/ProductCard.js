import { Card } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';
import { useState } from 'react';
import { useEffect } from 'react';
import Stars from './Stars';

export default function ProductCard(props) {
  const { name, imageUrl, price, ratings, rating, slug, numSold, isActive, _id }
    = props.product;
  const { admin } = props

  const href = admin ? `/admin/products/edit/${_id}` : `/product/${slug}`;
  return (
    <Card className="p-2 product-card border-0" as={Link} to={href}>
      <Card.Img className="product-card-image" variant="top" src={imageUrl} />
      <Card.Title className="py-2 product-card-title">{name}</Card.Title>
      {isActive && admin && <Badge bg="success">Available</Badge>}
      {!isActive && admin && <Badge bg="danger">Not Available</Badge>}
      <Card.Text>
        &#8369; {price.toLocaleString('en-PH')}
        <span style={{display: "inline-block", "float": "right"}}>
          { (ratings.length === 0) ? <></> : <Stars rating={rating}  /> }
          { (numSold === 0) ? <></> : <span> {numSold} sold </span> }
        </span>
        
      </Card.Text>
    </Card>
  )
};

// vim: ft=javascriptreact
