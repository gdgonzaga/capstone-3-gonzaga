import Swal from 'sweetalert2';

import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';

export default function AdminProductAdd() {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [isActive, setIsActive] = useState(false);
  const [imageUrl, setImageUrl] = useState('');
  const [isValid, setIsValid] = useState(false);

  const navigate = useNavigate();

  useEffect(() => {
    if (
      name.length > 0 &&
      description.length > 0 &&
      price > 0
    )
      setIsValid(true)
    else
      setIsValid(false)

  }, [name, description, price, isActive, imageUrl]);

  const [textareaRows, setTextareaRows] = useState('1');

  useEffect(() => {
    if (description.length <= 55)
      setTextareaRows(1);
    if (description.length > 55)
      setTextareaRows(4);
    if (description.length > 115)
      setTextareaRows(7);
    if (description.length > 150)
      setTextareaRows(10);
  }, [description]);

  const addProduct = (e) => {
    e.preventDefault();

  const productInfo =  {
      name: name,
      description: description,
      price: price,
      isActive: isActive,
      imageUrl: imageUrl
    }

    fetcher.addProduct(productInfo)
      .then(({status, data}) => {
        if (data._id) {
          Swal.fire({
            title: 'Product created successfully',
            icon: 'success',
            toast: true,
            timerProgressBar: true,
            timer: 2000,
            position: 'top'
          })
          navigate('/admin/products')
        }
        else
          Swal.fire({
            title: 'Cannot create product',
            icon: 'error',
            toast: true,
            timerProgressBar: true,
            timer: 2000,
            position: 'top'
          })
      })
  }

  return (
    <Container>
      <Row>
        <Col md={{ span: 6, offset: 3 }}>
          <Button onClick={() => { navigate(-1) } }>Back</Button>
          <h1>Add product</h1>
          <Form onSubmit={addProduct}>
            <Form.Group className="mb-3" controlId="name">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                onChange={(e) => setName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="description">
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                as="textarea"
                rows={textareaRows}
                onChange={(e) => setDescription(e.target.value)}
                onPaste={(e) => setDescription(e.target.value)}
                onCut={(e) => setDescription(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="price">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                onChange={(e) => setPrice(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="Image URL">
              <Form.Label>Image URL</Form.Label>
              <Form.Control
                type="string"
                onChange={(e) => setImageUrl(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="isActive">
              <Form.Label>Is active</Form.Label>
              <Form.Check
                type="switch"
                value="false"
                onChange={(e) => setIsActive(e.target.checked)}
              />
            </Form.Group>

            {isValid ? (
              <Button variant="success" type="submit" id="submitBtn">
                Submit
              </Button>
            ) : (
              <Button variant="light" type="submit" id="submitBtn" disabled>
                Submit
              </Button>
            )}
          </Form>
        </Col>
      </Row>
    </Container>
  )
};

// vim: ft=javascriptreact
