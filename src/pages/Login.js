import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useParams} from 'react-router-dom';
import Swal from 'sweetalert2';
import fetcher from '../fetcher';
import { Link } from 'react-router-dom';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState('');

  const { user, setUser, updateUserInfo } = useContext(UserContext);

  function authenticate(e) {
    e.preventDefault();

    fetcher.login(email, password)
      .then(({status, data}) => {
        if (typeof data.access !== 'undefined') {
          localStorage.setItem('token', data.access);
          localStorage.setItem('userId', data.id);
          retrieveUserDetails(data.id);

          Swal.fire({
            title: 'Log in successful',
            icon: 'success',
            toast: true,
            timerProgressBar: true,
            timer: 2000,
            position: 'top'
          });
        } else {
          Swal.fire({
            title: 'Authentication failed!',
            icon: 'error',
            toast: true,
            timerProgressBar: true,
            timer: 5000,
            position: 'top'
          })
        }
      });

    const retrieveUserDetails = (userId) => {
      fetcher.user(userId)
        .then(({status, data}) => {
          const tmp = data;
          tmp.id = data._id;
          setUser(tmp);
          updateUserInfo();
        });
    };

    // Set email of authed user in local storage
    //localStorage.setItem('email', email);

    //setUser({
      //email: localStorage.getItem('email'),
    //});

    //alert('You are now logged in.');
    setEmail('');
    setPassword('');
  }

  useEffect(() => {
    if (email !== '' && password !== '') setIsActive(true);
    else setIsActive(false);
  }, [email, password]);

  let redirectUrl = '';
  const { returnUrl } = useParams();
  if (returnUrl)
    redirectUrl = '/' + decodeURIComponent(returnUrl);
  else
    redirectUrl = '/'

  return (user) ? (<Navigate to={redirectUrl} />) : (
    <Container>
      <Row>
        <Col md={{ span: 6, offset: 3 }} className="border rounded py-3">
          <h1>Log in</h1>
          <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group className="mb-3" controlId="userEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </Form.Group>
            {isActive ? (
              <Button variant="success" type="submit" id="submitBtn">
                Submit
              </Button>
            ) : (
              <Button variant="muted" type="submit" id="submitBtn" disabled>
                Submit
              </Button>
            )}
          </Form>
            <div className="mt-3">Do not have an account? <Link to={"/register"}>Register here </Link></div>
        </Col>
      </Row>
    </Container>
  );
}
