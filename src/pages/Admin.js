import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';
import { Nav } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';

import AdminProducts from '../components/AdminProducts';
import AdminOrders from '../components/AdminOrders';
import PageNotFound from './PageNotFound';
import LogInPrompt from '../components/LogInPrompt';
import AdminProductAdd from './AdminProductAdd';
import AdminProductEdit from './AdminProductEdit';
import AccountProfile from './AccountProfile';
import AdminUsers from './AdminUsers';
import AdminReports from './AdminReports';

export default function Admin() {
  const { user } = useContext(UserContext);
  const [ sectionComponent, setSectionComponent ] = useState();

  const navigate = useNavigate();
  const { param1, param2, param3 } = useParams();

  useEffect(() => {
    if (!param1)
      navigate('/admin/reports')
    else if (param1 === 'reports')
      setSectionComponent(<AdminReports />)
    else if (param1 === 'orders')
      setSectionComponent(<AdminOrders />)

    else if (param1 === 'products' && param2 === 'edit' && param3)
      setSectionComponent(<AdminProductEdit productId={param3} />)
    else if (param1 === 'products' && param2 === 'edit')
      setSectionComponent(<h1>users</h1>)
    else if (param1 === 'products' && param2 === 'addProduct')
      setSectionComponent(<AdminProductAdd />)
    else if (param1 === 'products' && !param2)
      setSectionComponent(<AdminProducts />)

    else if (param1 === 'users' && param2)
      setSectionComponent(<AccountProfile userId={param2} admin={true} />)
    else if (param1 === 'users')
      setSectionComponent(<AdminUsers />)
    else
      setSectionComponent(<PageNotFound />);
  }, [param1, param2, param3])

  let toRender = <></>;

  if (!user)
    toRender = <LogInPrompt returnUrl={window.location.pathname} />;
  else if (user && !user.isAdmin)
    toRender = <h1>This page is for admin users only</h1>
  else
    toRender =
      <>
        <Nav className="justify-content-center">
            <h2 className="justify-content-center">Admin dashboard</h2>
        </Nav>

        <Nav justify variant="tabs" defaultActiveKey={param1 ? param1: 'reports'} className="justify-content-center">
          <Nav.Item>
            <Nav.Link variant="success" eventKey="reports" onClick={e => navigate('/admin/reports')}>Reports</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="orders" onClick={e => navigate('/admin/orders')}>Orders</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="products" onClick={e => navigate('/admin/products')}>Products</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="users" onClick={e => navigate('/admin/users')}>Users</Nav.Link>
          </Nav.Item>
        </Nav>
        <Container className="border border-top-0 p-3">
          { sectionComponent }
        </Container>
      </>

  return toRender;
};

// vim: ft=javascriptreact
