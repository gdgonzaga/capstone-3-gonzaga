import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';

export default function AdminReports() {
  const [itemsSold, setitemsSold] = useState(0);
  const [itemsSoldLast30, setitemsSoldLast30] = useState(0);
  const [amountSold, setamountSold] = useState(0);
  const [amountSoldLast30, setamountSoldLast30] = useState(0);
  const [pendingOrders, setpendingOrders] = useState(0);
  const [refundRequests, setrefundRequests] = useState(0);
  const [totalOrders, settotalOrders] = useState(0);
  const [totalUsers, settotalUsers] = useState(0);

  function getItemsSold(orders) {
    function getOrderItemsSold(order) {
      return order.products.reduce((accumulator, op) => {
        return accumulator + op.quantity
      }, 0)
    }

    return orders.reduce((accumulator, order) => {
      return accumulator + (getOrderItemsSold(order))
    }, 0)
  }

  function getAmountSold(orders) {
    return orders.reduce((accumulator, order) => {
      return accumulator + order.totalAmount;
    }, 0)
  }

  function filterOrdersInLast30(orders) {
    return orders.filter(order => {
              const purchaseTimeStamp = (new Date(order.purchasedOn).getTime())
              const timestamp30DaysAgo = (Date.now() - (60*60*24*30*1000))
              return (timestamp30DaysAgo < purchaseTimeStamp)
            })
  }

  function getPendingOrders(orders) {
    return orders.reduce((accumulator, order) => {
      if (!order.packedOn)
        return accumulator + 1;
      else
        return accumulator;
    }, 0)
  }

  function getRefundRequests(orders) {
    return orders.reduce((accumulator, order) => {
      if (order.refundRequestedOn && !order.refundApprovedOn)
        return accumulator + 1;
      else
        return accumulator;
    }, 0)
  }

  useEffect(() => {
    fetcher.orders()
      .then(({status, data: orders}) => {
        setitemsSold(getItemsSold(orders));
        setamountSold(getAmountSold(orders));
        setitemsSoldLast30(getItemsSold(filterOrdersInLast30(orders)));
        setamountSoldLast30(getAmountSold(filterOrdersInLast30(orders)));
        setpendingOrders(getPendingOrders(orders));
        setrefundRequests(getRefundRequests(orders));
        settotalOrders(orders.length)
      })
      .then(() => {
        fetcher.users()
          .then(({status, data: users}) => {
            settotalUsers(users.length)
          })
      })
  }, []);

  return (
    <>
      <div>
        <ul className="list-unstyled">
          <li>
            <strong>
              Total items sold:
            </strong>
            <small className="mx-2">
              {itemsSold.toFixed()}
            </small>
          </li>
          <li>
            <strong>
              Total amount sold:
            </strong>
            <small className="mx-2">
              &#8369; {amountSold.toLocaleString()}
            </small>
          </li>
          <li>
            <strong>
              Items sold in the past 30 days:
            </strong>
            <small className="mx-2">
              {itemsSoldLast30}
            </small>
          </li>
          <li>
            <strong>
              Total amount sold in the past 30 days:
            </strong>
            <small className="mx-2">
              &#8369; {amountSoldLast30.toLocaleString()}
            </small>
          </li>
          <li>
            <strong>
              Pending orders:
            </strong>
            <small className="mx-2">
              {pendingOrders}
            </small>
          </li>
          <li>
            <strong>
              Refund requests:
            </strong>
            <small className="mx-2">
              {refundRequests}
            </small>
          </li>
          <li>
            <strong>
              Total orders:
            </strong>
            <small className="mx-2">
              {totalOrders}
            </small>
          </li>
          <li>
            <strong>
              Total users:
            </strong>
            <small className="mx-2">
              {totalUsers}
            </small>
          </li>
        </ul>
      </div>
    </>
  );
};

// vim: ft=javascriptreact
