import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';

import AccountProfile from './AccountProfile';
import AdminUserLine from '../components/AdminUserLine';

export default function AdminUsers() {
  const [components, setComponents] = useState()

  function buildComponent(users) {
    return users.map(user => (
      <li className="my-1" key={user._id}><AdminUserLine user={user} /></li>
    ))
  }

  useEffect(() => {
    fetcher.users()
      .then(({ status, data }) => {
        if (status === 200)
          setComponents(buildComponent(data));
      })
  }, []);


  return (
    <>
      {components}
    </>
  )
};

// vim: ft=javascriptreact
