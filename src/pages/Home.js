import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';

import ProductGallery from '../components/ProductGallery';
import PageNotFound from './PageNotFound';
import Search from './Search';

import { Nav } from 'react-bootstrap';
import BestSellingProducts from '../components/BestSellingProducts';

export default function Home() {
  const navigate = useNavigate();
  const { param1, param2 } = useParams();

  const [products, setProducts] = useState([]);
  const [productsReverse, setProductsReverse] = useState([]);
  const [section, setSection] = useState('');

  function getActiveProducts() {
    fetcher.activeProducts()
      .then(({status, data}) => {
        if (status === 200)
          setProducts(data);
          setProductsReverse(data.reverse());
      })
  }

  const[activeHome, setActiveHome] = useState('');
  const[activeProducts, setActiveProducts] = useState('');

  function activateHome() {
    setActiveProducts('');
    setActiveHome('active');
  }

  function clickedHome() {
    activateHome();
    navigate('/home');
  }

  function activateProducts() {
    setActiveProducts('active');
    setActiveHome('');
  }

  function clickedProducts() {
    activateProducts()
    navigate('/home/products');
  }

  useEffect(() => {
    if (!param1 || param1 === 'home') {
      getActiveProducts();
      activateHome();
      setSection('home');
    }
    else if (param1 === 'products') {
      getActiveProducts();
      activateProducts();
      setSection('products');
    }
    else if (param1 === 'search' && param2) {
      activateProducts();
      setSection('search');
    }
    else
      setSection('pageNotFound');
  }, [param1, param2]);

  return (
    <>
        <Nav className="justify-content-center">
            <h2>Shop now!</h2>
        </Nav>
        <Nav justify variant="tabs"
          className="justify-content-center">
          <Nav.Item>
            <Nav.Link className={activeHome} eventKey="home" onClick={clickedHome}>
              <span className="text-warning">
                <i className="fa-solid fa-fire-flame-curved"></i></span>&nbsp;
                <span>Hot items</span>&nbsp;
                <span className="text-warning"><i className="fa-solid fa-fire-flame-curved"></i>
              </span>
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link className={activeProducts} eventKey="allProducts" onClick={clickedProducts}>Products</Nav.Link>
          </Nav.Item>
        </Nav>
        <Container className=" p-3 border border-top-0">
          {section === 'home' && <BestSellingProducts products={products} />}
          {section === 'products' && <><h2>All products</h2><ProductGallery products={products} /></>}
          {section === 'search' && <Search term={param2}/>}
          {section === 'pageNotFound' && <PageNotFound />}
        </Container>
    </>
  )
};

// vim: ft=javascriptreact
