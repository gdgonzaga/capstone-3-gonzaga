import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';

import { swalError } from '../utils';
import CartProductCard from '../components/CartProductCard';
import WishlistProductCard from '../components/WishlistProductCard'

export default function Wishlist() {
  const [cart, setCart] = useState([]);
  const [products, setProducts] = useState([]);
  const { updateUserInfo, user } = useContext(UserContext);

  useEffect(() => {
    updateUserInfo();
  }, []);

  useEffect(() => {
    const productIds = user.wishlist.map(wp => wp.productId);

    fetcher.productsFromIdList(productIds)
      .then(({status, data}) => {
        setProducts(data);
      })
  }, [user]);

  const [component, setComponent] = useState(<></>);

  useEffect(() => {
    if (products.length > 0)
      setComponent(products.map(p => {
        return (
          <div className="d-flex align-items-stretched" style={{maxWidth: "250px", margin: "0.5rem"}}>
            <WishlistProductCard product={p} />
          </div>
        )
      }
      ));
  }, [products]);

  return products.length === 0 ? <h3>Your wish list is empty</h3> : (
    <>
      <div className="d-flex justify-content-center flex-wrap">
          {component}
      </div>
    </>
  )
};

// vim: ft=javascriptreact
