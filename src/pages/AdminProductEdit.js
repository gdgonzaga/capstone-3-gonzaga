import Swal from 'sweetalert2';

import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import EditField from '../components/EditField';
import fetcher from '../fetcher';
import { swalSuccess } from '../utils';
import { swalError } from '../utils';

  //name: {
  //description: {
  //price: {
  //isActive: {
  //stock: {
  //createdOn: {
  //imageUrl: {
  //rating: {
  //ratings: [
      //stars: {
      //orderId: {
  //numSold: {
  //slug: String,

export default function AdminProductEdit({ productId }) {
  const navigate = useNavigate();
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [isActive, setIsActive] = useState(true);
  const [imageUrl, setImageUrl] = useState('');
  const [stock, setStock] = useState('');

  const [isValid, setIsValid] = useState(false);

  const [rating, setRating] = useState();
  const [numRatings, setNumRatings] = useState();
  const [slug, setSlug] = useState();
  const [numSold, setNumSold] = useState();

  const [product, setProduct] = useState({});

  useEffect(() => {
    fetcher.product(productId)
      .then(({status, data}) => {
        setProduct(data);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setIsActive(data.isActive);
        setImageUrl(data.imageUrl);
        setStock(data.stock);

        setRating(data.rating);
        setNumRatings(data.ratings.length);
        setSlug(data.slug);
        setNumSold(data.numSold);
      })
  }, []);

  function sendUpdate(field) {
    fetcher.updateProduct(product._id, field).then(({ status, data }) => {
      if (status === 200)
        swalSuccess('Successfully updated');
      else
        swalError('Cannot update user');
    });
  }

  function updateName(value) { sendUpdate({ name: value }, setName(value))}
  function updateDescription(value) { sendUpdate({ description: value }, setDescription(value))}
  function updatePrice(value) { sendUpdate({ price: value }, setPrice(value))}
  function updateIsActive(value) { sendUpdate({ isActive: value }, setIsActive(value))}
  function updateStock(value) { sendUpdate({ stock: value }, setStock(value))}
  function updateImageUrl(value) { sendUpdate({ imageUrl: value }, setImageUrl(value))}


  return (
    <Container>
      <Row>
        <Col md={{ span: 8, offset: 2 }}>
          <h1>Edit product</h1>
          <ul className="editable-list list-unstyled">
            <li><Button onClick={() => { navigate(-1) } }>Back</Button></li>
            <li><EditField value={name} label={"Name"} onConfirm={updateName} /></li>
            <li><EditField value={description} label={"Description"} onConfirm={updateDescription} /></li>
            <li><EditField value={price} label={"Price"} onConfirm={updatePrice} /></li>
            <li><EditField value={imageUrl} label={"Image URL"} onConfirm={updateImageUrl} /></li>
            <li><EditField value={stock} label={"Stock"} onConfirm={updateStock} /></li>
            <li><EditField value={isActive} label={"IsActive"} onConfirm={updateIsActive} /></li>
            <li>
              <strong>Product link:{' '}</strong>
              <Link
                to={`/product/${slug}`}
              >{`${fetcher.FRONTEND_BASE_URL}/product/${slug}`}</Link>
            </li>
            <li>
              <strong>Rating:</strong> <small>{rating && rating.toFixed(2)}</small>
            </li>
            <li>
              <strong>Ratings:</strong> <small>{numRatings}</small>
            </li>
            <li>
              <strong>Sold:</strong> <small>{numSold}</small>
            </li>
            <li>
              <strong>Slug:</strong> <small>{slug}</small>
            </li>
          </ul>
        </Col>
      </Row>
    </Container>
  );
};

// vim: ft=javascriptreact
