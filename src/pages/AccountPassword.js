import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import { swalSuccess } from '../utils';
import { swalError } from '../utils';
import { Modal } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';

export default function AccountPassword() {
  const { user } = useContext(UserContext);
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [formIsValid, setFormIsValid] = useState(false);

  function submit(e)  {
    e.preventDefault();
    fetcher.updateUser(user._id, { password: password1 })
      .then(({status, data}) => {
        if (status === 200)
          swalSuccess('Successfully changed password')
        else
          swalError('Cannot change password')
    })
    setIsVisible(false);
  }

  useEffect(() => {
    setFormIsValid(password1 && password1 === password2);
  }, [password1, password2]);

  const [isVisible, setIsVisible] = useState(false);

  return (
    <>
      <Button onClick={() => setIsVisible(true)}>Change password</Button>

      <Modal show={isVisible} onHide={() => setIsVisible(false)}>
        <Modal.Header>
          Set password
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={e => submit(e)}>
            <Form.Group className="mb-3">
              <Form.Label>New Password</Form.Label>
              <Form.Control
                required
                type="password"
                onChange={(e) => setPassword1(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control
                required
                type="password"
                onChange={(e) => setPassword2(e.target.value)}
              />
              <Form.Text className="text-muted">
                Confirm your new password by entering it again.
              </Form.Text>
            </Form.Group>

            {formIsValid ? (
              <Button variant="success" type="submit" onSubmit={e => submit(e)}>
                Save changes
              </Button>
            ):(
              <Button variant="light" disabled type="submit">
                Save changes
              </Button>

            )
            }
          </Form>
        </Modal.Body>
      </Modal>
    </>
  )
};

// vim: ft=javascriptreact
