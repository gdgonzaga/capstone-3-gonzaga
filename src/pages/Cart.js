import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';
import { createContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';
import { swalError } from '../utils';

import ProductCard from '../components/ProductCard';
import CartProductCard from '../components/CartProductCard';
import { swalSuccess } from '../utils';

export const CartContext = createContext();

export default function Cart() {
  const [cart, setCart] = useState([]);
  const [products, setProducts] = useState([]);
  const { updateUserInfo } = useContext(UserContext);
  const navigate = useNavigate();


  function updateCart() {
    fetcher.cart()
      .then(({status, data}) => {
        if (status !== 200) {
          swalError(data.message || 'Cannot get data from server.', 5000);
          return;
        }

        setCart(data);
        fetcher.orderProducts(data._id)
          .then(({ data }) => {
            setProducts(data);
          })
      })
  };

  function placeOrder() {
    fetcher.checkOut()
      .then(({ status }) => {
        if (status === 200) {
          updateUserInfo();
          updateCart();
          swalSuccess('Order sucessfully placed', 4000)
          navigate("/account/orders");
        }
      })
  }

  useEffect(() => {
    updateCart();
  }, []);

  const [component, setComponent] = useState(<></>);

  useEffect(() => {
    if (products.length > 0)
      setComponent(products.map(p => {
        let quantity = null;

        cart.products.forEach(op => {
          if (op.productId === p._id)
            quantity = op.quantity;
        })

        return (
          <div style={{maxWidth: "250px", margin: "0.5rem"}} className="d-flex align-items-stretched">
            <CartProductCard product={p} quantity={quantity} />
          </div>
        )
      }
      ));
  }, [products]);

  return products.length === 0 ? (
    <h3>Your cart is empty</h3>
  ) : (
    <>
      <div className="d-flex justify-content-center flex-wrap">
        <Button
          variant="success"
          className="centered-child px-5"
          onClick={placeOrder}
        >
          Place order
        </Button>
      </div>
      <CartContext.Provider value={{ cart, updateCart }}>
        <div className="d-flex justify-content-center flex-wrap">
          <h3>Total: &#8369; {cart.totalAmount.toLocaleString()}</h3>
        </div>
        <div className="d-flex justify-content-center flex-wrap">
          {component}
        </div>
      </CartContext.Provider>
    </>
  );
};

// vim: ft=javascriptreact
