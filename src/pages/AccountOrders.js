import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';
import OrderGallery from '../components/OrderGallery';
import fetcher from '../fetcher';

export default function AccountOrders() {
  const [ orders, setOrders ] = useState([]);

  useEffect(() => {
    fetcher.orders()
      .then(({status, data}) => {
        setOrders(data.reverse());
      });
  }, []);

  return (
    <>
      <OrderGallery orders={orders}/>
    </>
  )
};

// vim: ft=javascriptreact
