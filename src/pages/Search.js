import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import fetcher from '../fetcher';
import ProductGallery  from '../components/ProductGallery';

export default function Search({ term: encodedSearch}) {
  const [ products, setProducts ] = useState([]);

  useEffect(() => {
    fetcher.search(encodedSearch)
      .then(({status, data}) => {
        setProducts(data);
      });
  }, [])

  return (
    <>
      <h2>Search results</h2>
      <ProductGallery products={products} />
    </>
  )
};
